show databases;
use test1;

-- TABEL GAME
describe games;
drop table games;
create table games
(
    id           int primary key not null auto_increment,
    name         varchar(255)    not null,
    genre_id     int,
    numb_players int,
    price        int,
    off_on       bit,
    description  varchar(255),
    platform_id  int
);
SELECT *
from games;

insert into games(name, genre_id, numb_players, price, off_on, description, platform_id)
values ('Minecraft', 2, 1, 50, true, 'blokjes', 1),
       ('Runescape', 1, 1, 1, false, 'oldschool', 2),
       ('Skyrim', 3, 1, 20, true, 'middeleeuwen', 3),
       ('Sims', 4, 1, 80, false, 'leven', 2),
       ('Overwatch', 1, 0, 2, true, 'schiet', 1);

update games
set genre_id    = 1,
    platform_id = 2
where id = 1;
-- TABLE GENRE

describe genre;
drop
    table genre;
create table genre
(
    id          int primary key not null auto_increment,
    name        varchar(56),
    description varchar(255)

);
select *
from genre;
insert into genre(name, description)
values ('Puzzel', 'Alle strategische games'),
       ('Action', 'Alle spannende games'),
       ('Adventure', 'Alle avontuur games'),
       ('Shooter', 'Alle schiet games'),
       ('Casual', 'Alle rustige games');

-- TABEL PLATFORM
describe platform;
drop
    table platform;
create table platform
(
    id   int primary key not null auto_increment,
    name varchar(56)
);
select *
from platform;
insert into platform(name)
values ('Xbox'),
       ('Playstation'),
       ('Switch'),
       ('PC'),
       ('Mobiel');

-- TABEL USER
describe user;
drop
    table user;
create table user
(
    id    int primary key not null auto_increment,
    name  varchar(64)     not null,
    email varchar(64)
);
select *
from user;
insert into user(name, email)
values ('HenkHolbewoner', 'Henkholbewoner@game_fanaat.nl'),
       ('Antonaso', 'Antonaso@game_fanaat.nl'),
       ('Lucasleipo', 'Lucasleipo@game_fanaat.nl'),
       ('KatjaKrijger', 'KatjaKrijger@game_fanaat.nl'),
       ('WillemWarlord', 'WillemWarlord@game_fanaat.nl');

-- TABLE GAME_USER
describe game_user;
drop
    table game_user;
create table game_user
(
    id      int primary key not null auto_increment,
    game_id int,
    user_id int
);

insert into game_user(game_id, user_id)
values (1, 1),
(1, 2),
(2, 3),
(5, 6);
select *
from game_user;


# -- JOINS
# select game.name, genre.name, platform.name
# from game
#          join platform p on platform.id = game.platform_id
#          join genre on genre_id = game.genre_id;
#
# select game.name, user.name from user
# join game_user gu on game.id = gu.game_id;
select user.name, g.name from user
join game_user gu on user.id = gu.user_id
join games g on gu.game_id = g.id


